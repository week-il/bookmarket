package net.tncy.bookmarket.services;

import net.tncy.bookmarket.data.Book;
import net.tncy.bookmarket.data.BookFormat;

public class BookService {
    private int id = 0;
    private Book[] booklist;
    private int createBook(String title, String author, String publisher, String ISBN){
        Book book = new Book(id, title, author, publisher, BookFormat.POCHE, ISBN);
        booklist[id] = book;
        id++;
        return 0;
    }
    private Book[] getBooks(){
        return booklist;
    }
}
