package net.tncy.bookmarket.data;

import net.tncy.dba.validator.ISBN;

public class Book {
    private int id;
    private String title;
    private String author;
    private String publisher;
    private BookFormat format;
    @ISBN
    private String isbn;

    public Book(int id, String title, String author, String publisher, BookFormat format, String isbn) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.format = format;
        this.isbn = isbn;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return this.publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public BookFormat getFormat() {
        return this.format;
    }

    public void setFormat(BookFormat format) {
        this.format = format;
    }

    public String getIsbn() {
        return this.isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
}
