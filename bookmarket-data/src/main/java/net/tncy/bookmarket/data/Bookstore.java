package net.tncy.bookmarket.data;

public class Bookstore {
    private int id;
    private String name;
    private InventoryEntry[] inventoryEntries;

    public Bookstore(int id, String name, InventoryEntry[] inventoryEntries) {
        this.id = id;
        this.name = name;
        this.inventoryEntries = inventoryEntries;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public InventoryEntry[] getInventoryEntries() {
        return this.inventoryEntries;
    }

    public void setInventoryEntries(InventoryEntry[] inventoryEntries) {
        this.inventoryEntries = inventoryEntries;
    }
}
